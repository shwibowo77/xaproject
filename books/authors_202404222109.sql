INSERT INTO public.authors ("name",nick_name,nationality,born) VALUES
	 ('John Darnton','JD','USA',1941),
	 ('Agatha Christie','AC','UK',1890),
	 ('J.K Rowling','JK','UK',1965),
	 ('Charles Dickens','Charles','UK',1812),
	 ('Ian Fleming','Ian','UK',1908);
