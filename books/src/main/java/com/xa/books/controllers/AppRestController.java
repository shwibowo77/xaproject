package com.xa.books.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.books.models.Authors;
import com.xa.books.models.Books;
import com.xa.books.repositories.AuthorsRepo;
import com.xa.books.repositories.BooksRepo;

@RestController
@RequestMapping(value="books")
@CrossOrigin("*")
public class AppRestController {
	
	@Autowired private BooksRepo booksRepo;
	@Autowired private AuthorsRepo authorsRepo;
	
	@GetMapping("authors")
	public ResponseEntity<List<Authors>> getAllAuthors() {
		List<Authors> authors = this.authorsRepo.findAll();
		return new ResponseEntity<List<Authors>>(authors, HttpStatus.OK);
	}
	
	@GetMapping()
	public ResponseEntity<List<Books>> getAllBooks() {
		List<Books> books = this.booksRepo.findAll();
		return new ResponseEntity<List<Books>>(books, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getAllBooksById(@PathVariable("id") Long id) {
		Optional<Books> books = this.booksRepo.findById(id);
		return new ResponseEntity<>(books, HttpStatus.OK);
	}
	
}
