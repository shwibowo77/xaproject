package com.xa.books.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.books.models.Authors;

@Repository
public interface AuthorsRepo extends JpaRepository<Authors, Long>{

}
