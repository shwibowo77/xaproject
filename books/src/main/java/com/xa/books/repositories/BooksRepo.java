package com.xa.books.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.books.models.Books;

@Repository
public interface BooksRepo extends JpaRepository<Books, Long> {

}
