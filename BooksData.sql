insert into authors (name, nick_name, nationality, born) values
('John Darnton', 'JD', 'USA', 1941),
('Agatha Christie', 'AC', 'UK', 1890),
('J.K Rowling', 'JK', 'UK', 1965),
('Charles Dickens', 'Charles', 'UK', 1812),
('Ian Fleming', 'Ian', 'UK', 1908);

INSERT INTO public.books (isbn,title,"year",author_id) VALUES
	 ('0679449787','Neanderthal',1996,1),
	 ('978-1400041374','The Darwin Conspiracy',2005,1),
	 ('978-602-979-222-889','Death in the clouds',1935,2),
	 ('9789792229806','Murder on the orient express',1934,2),
	 ('0061002798','The Clocks',1963,2),
	 ('978-0-7515-6227-9','Career of Evil',2015,3),
	 ('978-0-7515-8420-2','The Ink Black Heart',2022,3),
	 ('91-1-937201-9','Oliver Twist',1838,4),
	 ('0140430083','David Copperfield',1850,4),
	 ('9780141439723','Bleak House',1852,4),
	 ('1612185436','Casino Royale',1953,5),
	 ('1612185509','Goldfinger',1959,5),
	 ('0142002070','From Russia with Love',1957,5),
	 ('0142003247','Thunderball',1961,5);








