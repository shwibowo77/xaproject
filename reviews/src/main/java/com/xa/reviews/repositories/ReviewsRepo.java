package com.xa.reviews.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.reviews.models.Reviews;
import java.util.List;


@Repository
public interface ReviewsRepo extends JpaRepository<Reviews, Long> {

	List<Reviews> findByBookId(Long bookId);
	
}
