package com.xa.reviews.models;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="reviews")
public class Reviews {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="book_id")
	private Long bookId;
	
	@Column(name="email")
	private String Email;
	
	@Column(name="post_at")
	private Date postAt;
	
	@Column(name="review", length = 1000)
	private String Review;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Date getPostAt() {
		return postAt;
	}

	public void setPostAt(Date postAt) {
		this.postAt = postAt;
	}

	public String getReview() {
		return Review;
	}

	public void setReview(String review) {
		Review = review;
	}
	
}
