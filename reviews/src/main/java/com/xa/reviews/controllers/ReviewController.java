package com.xa.reviews.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="books-ui")
public class ReviewController {

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("index");
		return view;
	}
	
}
