package com.xa.reviews.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.reviews.models.Reviews;
import com.xa.reviews.repositories.ReviewsRepo;

@RestController
@RequestMapping(value="reviews")
@CrossOrigin("*")
public class AppRestController {
	
	@Autowired private ReviewsRepo reviewsRepo;
	
	@GetMapping("/{book_id}")
	public ResponseEntity<List<Reviews>> getReviews(@PathVariable("book_id") Long book_id) {
		List<Reviews> reviews = this.reviewsRepo.findByBookId(book_id);
		return new ResponseEntity<List<Reviews>>(reviews, HttpStatus.OK);
	}

}
