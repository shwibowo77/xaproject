INSERT INTO public.reviews (email,review,book_id,post_at) VALUES
	 ('me@gmail.com','The books with knowledges',1,'2024-04-23 03:54:56.036'),
	 ('you@me.com','The historical knowledge',1,'2024-04-23 04:04:09.956'),
	 ('alpha@hotmail.net','Monkeys conspiracy',2,'2024-04-23 04:05:18.282'),
	 ('beta@gmail.com','Very intresting books, the tricks is amazing...',4,'2024-04-23 04:16:27.932'),
	 ('gamma@mail.com','Another James Bond sequel',11,'2024-04-23 04:50:16.63'),
	 ('delta@linux.com','the legend was here',9,'2024-04-23 04:52:27.267');
